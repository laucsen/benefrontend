import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from 'react-highlight-words';

import './index.scss';

class SmartSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      selected: false,
    };
  }

  updateAndFireEvents(value) {
    const { onChange, onUnselect } = this.props;
    const { selected } = this.state;
    if (selected) {
      onUnselect();
    }
    this.setState({ value, selected: false });
    onChange(value);
  }

  selectItem(value) {
    const { onSelection } = this.props;
    this.setState({ value, selected: true }, () => onSelection(value));
  }

  render() {
    const { value } = this.state;
    const { options, limit } = this.props;
    const procOptions = options
      .sort((a, b) => {
        if (a < b) { return -1; }
        if (a > b) { return 1; }
        return 0;
      })
      .splice(0, limit);

    return (
      <div className="smart-select">
        <input
          className="smart-select__input"
          placeholder="Type your capital here..."
          type="text"
          value={value}
          onChange={ev => this.updateAndFireEvents(ev.target.value)}
        />
        <div className="smart-select__select-area">
          {procOptions.map(opt => (
            <div
              key={opt}
              onClick={() => this.selectItem(opt)}>
              <Highlighter
                className="smart-select__select-item"
                searchWords={[value]}
                textToHighlight={opt}
                highlightClassName="smart-select__option--highlight"
                unhighlightClassName="smart-select__option"
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

SmartSelect.defaultProps = {
  limit: -1,
};

SmartSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSelection: PropTypes.func.isRequired,
  onUnselect: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  limit: PropTypes.number,
};

export default SmartSelect;
