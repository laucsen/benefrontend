import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import ClipLoader from 'react-spinners/ClipLoader';

const PrivateRoute = ({
  component: Component,
  isLogged,
  isAuthenticating,
  ...rest
}) => (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticating) {
          return (
            <div>
              <ClipLoader
                sizeUnit={'px'}
                size={150}
                color={'#123abc'}
                loading={true}
              />
            </div>
          );
        } else if (isLogged) {
          return <Component {...props} {...rest.extra} />;
        }

        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        );
      }}
    />
  );

const mapStateToProps = (state) => ({
  isLogged: state.auth.isLogged,
  isAuthenticating: state.auth.isAuthenticating,
});

export default connect(
  mapStateToProps,
  null
)(PrivateRoute);
