import { handleActions } from 'redux-actions';
import {
  validateFailed,
  loginRequested,
  loginFailed,
  loginSucceeded,
} from './actions';

const defaultState = {
  token: undefined,
  currentUser: undefined,
  //
  isLogged: false,
  isAuthenticating: true,
  //
};

export default handleActions({
  [validateFailed]: state => ({
    ...state,
    isLogged: false,
    isAuthenticating: false,
    token: undefined,
    currentUser: undefined,
  }),
  [loginSucceeded]: (state, { payload }) => ({
    ...state,
    token: payload.token,
    currentUser: payload.user,
    isLogged: true,
    isAuthenticating: false,
  }),
  [loginRequested]: state => ({
    ...state,
    isAuthenticating: true,
  }),
  [loginFailed]: state => ({
    ...state,
    isLogged: false,
    isAuthenticating: false,
    token: undefined,
    currentUser: undefined,
  }),
}, defaultState);
