import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

// Reducers
import auth from '../features/auth/reducer';
import cities from '../features/cities/reducer';
// --------

export default combineReducers({
  auth,
  cities,
  //
  routing,
});
