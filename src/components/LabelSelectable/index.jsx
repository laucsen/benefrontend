import React from 'react';
import Select from 'react-select';
import fetch from 'isomorphic-fetch';

import './index.scss';

import APP_URL from '../../config/appurl';

const URL_TO_FETCH = `${APP_URL}/public/geolocation/contries`;

class LabelSelectable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.country,
      options: [],
    };
  }

  componentDidMount = async () => {
    const res = await fetch(URL_TO_FETCH, {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Content-Security-Policy': "default-src 'self'",
      'X-Frame-Options': 'SAMEORIGIN',
      'X-XSS-Protection': 1,
    });

    const result = await res.json();
    const mapped = result.map((opt) => ({
      value: opt,
      label: opt,
    }));
    this.setState({
      options: mapped,
    });
  };

  render() {
    const { text, name, onChange } = this.props;
    const { value, options } = this.state;
    return (
      <div className="label-selectable">
        <label className="label-selectable__label" htmlFor={name}>
          {text}
        </label>
        <Select
          value={value}
          onChange={(ev) => {
            this.setState({ value: ev });
            onChange(ev.label);
          }}
          options={options}
        />
      </div>
    );
  }
}

export default LabelSelectable;
