import React from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

import './index.scss';

const WeatherSpinner = () => (
  <div className="weather-spinner">
    <ClipLoader
      sizeUnit={'px'}
      size={150}
      color={'#93B7CC'}
      loading
    />
  </div>
);

export default WeatherSpinner;
