import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './index.scss';

const ReturnButton = ({ to }) => (
  <div className="return-button--floating-top-left">
    <Link to={to}>
      <FontAwesomeIcon
        color="#93B7CC"
        size="2x"
        icon="less-than"
      />
    </Link>
  </div>
);

ReturnButton.propTypes = {
  to: PropTypes.string.isRequired,
};

export default ReturnButton;
