import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import ReturnButton from '../../../components/ReturnButton';
import SmartSelect from '../../../components/SmartSelect';
import WeatherSpinner from '../../../components/WeatherSpinner';

import { getCapitals, saveCapital } from '../actions';

import './cityCreation.scss';

class CityCreation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: false,
      selectedCity: '',
    };
  }

  updateListvalue(value) {
    const { getCapitals } = this.props;
    getCapitals(value);
  }

  cleanCapitals() {
    const { selected } = this.state;
    if (selected) {
      return [];
    }
    const { capitals, cities } = this.props;
    return capitals.filter((cap) => {
      for (let i = 0; i < cities.length; i += 1) {
        if (cities[i].name === cap) {
          return false;
        }
      }
      return true;
    });
  }

  selectItem(value) {
    this.setState({ selected: true, selectedCity: value });
  }

  unselect() {
    this.setState({ selected: false, selectedCity: '' });
  }

  requestSaveCapital() {
    const { selectedCity } = this.state;
    const { history, saveCapital } = this.props;
    saveCapital(selectedCity)
      .then(() => history.push('/'));
  }

  render() {
    const { loading } = this.props;
    if (loading) {
      return (<WeatherSpinner />);
    }
    
    const { selected } = this.state;

    const cleanCapitals = this.cleanCapitals();
    return (
      <div className="city-creation">
        <ReturnButton to="/" />
        <div className="city-creation__fit-area">
          <SmartSelect
            onChange={value => this.updateListvalue(value)}
            options={cleanCapitals}
            limit={8}
            onSelection={value => this.selectItem(value)}
            onUnselect={() => this.unselect()}
          />
          {selected && (
            <div className="city-creation__button-area">
              <div onClick={() => this.requestSaveCapital()} className="city-creation__button">Save</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

CityCreation.propTypes = {
  getCapitals: PropTypes.func.isRequired,
  saveCapital: PropTypes.func.isRequired,
  capitals: PropTypes.arrayOf(PropTypes.string).isRequired,
  cities: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
  })).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  capitals: state.cities.capitals,
  cities: state.cities.cities,
  loading: state.cities.loading,
});

const mapDispatchToProps = dispatch => ({
  getCapitals: value => dispatch(getCapitals(value)),
  saveCapital: value => dispatch(saveCapital(value)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CityCreation));
