/* eslint-disable */
import storage from 'redux-persist/es/storage';
import { persistReducer, persistStore } from 'redux-persist';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';

import rootReducer from './root-reducer';
import createBrowserHistory from 'history/createBrowserHistory';

import API from '../components/API';
import authTransform from './transforms';

const reducer = persistReducer({
  key: 'weather-test',
  storage,
  whitelist: ['auth'],
  transforms: [authTransform],
}, rootReducer);

const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(createBrowserHistory)),
    process.env.NODE_ENV !== 'production' &&
      typeof window === 'object' &&
      typeof window.devToolsExtension !== 'undefined'
      ? window.devToolsExtension()
      : (f) => f
  )
);

persistStore(store);
API.setStore(store);

export default store;
