import { createTransform } from 'redux-persist';

const authTransform = createTransform(
  (inboundState) => {
    const { token, currentUser } = inboundState;
    return { token, currentUser };
  },
  (outboundState) => {
    const { token, currentUser } = outboundState;
    return {
      token,
      currentUser,
      isLogged: (token !== undefined && token !== null && token !== ''),
      isAuthenticating: false,
    };
  },
  { whitelist: ['auth'] },
);

export default authTransform;
