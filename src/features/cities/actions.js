import { createAction } from 'redux-actions';
import { toast } from 'react-toastify';

import { invalidateAuth } from '../auth/actions';

import {
  CITIES,
  CAPITALS,
  SAVE_CITY,
  WEATHER,
} from './urls';
import API from '../../components/API';


// Actions we dispatch in our Action Creators

export const getCitiesRequested = createAction('CITIES/GET_CITIES_REQUESTED');
export const getCitiesFailed = createAction('CITIES/GET_CITIES_FAILED');
export const getCitiesSucceded = createAction('CITIES/GET_CITIES_SUCCEDED');

export const capitalsRequested = createAction('CITIES/CAPITALS_REQUESTED');
export const capitalsRequestFailed = createAction('CITIES/CAPITALS_REQUEST_FAILED');
export const capitalsRequestSucceded = createAction('CITIES/CAPITALS_REQUEST_SUCCEDED');

export const resetCapitals = createAction('CITIES/RESET_CAPITALS');

export const saveCapitalRequested = createAction('CITIES/SAVE_CAPITALS_REQUESTED');
export const saveCapitalRequestFailed = createAction('CITIES/SAVE_CAPITALS_REQUEST_FAILED');
export const saveCapitalRequestSucceded = createAction('CITIES/SAVE_CAPITALS_REQUEST_SUCCEDED');

export const getWeatherRequested = createAction('CITIES/GET_WEATHER_REQUESTED');
export const getWeatherRequestFailed = createAction('CITIES/GET_WEATHER_REQUEST_FAILED');
export const getWeatherRequestSucceded = createAction('CITIES/GET_WEATHER_REQUEST_SUCCEDED');

// Action Creators

export const getCities = () => async (dispatch) => {
  dispatch(getCitiesRequested());
  const body = await API.get(CITIES);
  if (body.error) {
    toast.error('Impossible to recover user cities. Please login again.', { position: toast.POSITION.TOP_CENTER });
    await dispatch(getCitiesFailed({ error: body.error }));
    return dispatch(invalidateAuth());
  }
  return dispatch(getCitiesSucceded({ cities: body.cities }));
};

export const getCapitals = value => async (dispatch) => {
  dispatch(capitalsRequested(value));
  if (value === undefined || value === null || value === '') {
    return dispatch(resetCapitals());
  }

  const body = await API.get(`${CAPITALS}?filter=${value}`);
  if (body.error) {
    toast.error('Impossible to recover capitals. Please login again.', { position: toast.POSITION.TOP_CENTER });
    await dispatch(capitalsRequestFailed({ error: body.error }));
    return dispatch(invalidateAuth());
  }
  return dispatch(capitalsRequestSucceded(body));
};

export const saveCapital = value => async (dispatch) => {
  dispatch(saveCapitalRequested(value));
  const body = await API.post(SAVE_CITY, {
    body: { name: value },
  });
  if (body.error) {
    toast.error('Impossible to save city. Please login again.', { position: toast.POSITION.TOP_CENTER });
    await dispatch(saveCapitalRequestFailed({ error: body.error }));
    return dispatch(invalidateAuth());
  }
  return dispatch(saveCapitalRequestSucceded(body));
};

export const getWeather = city => async (dispatch) => {
  dispatch(getWeatherRequested(city));
  const body = await API.get(`${WEATHER}${city}`);
  if (body.error) {
    toast.error('Impossible to recover weather of your city. Please login again.', { position: toast.POSITION.TOP_CENTER });
    await dispatch(getWeatherRequestFailed({ error: body.error }));
    return dispatch(invalidateAuth());
  }
  return dispatch(getWeatherRequestSucceded(body));
};
