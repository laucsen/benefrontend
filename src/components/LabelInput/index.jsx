import React from 'react';

import './index.scss';

const LabelInput = ({ text, type, name, onChange }) => (
  <div className="label-input">
    <label className="label-input__label" htmlFor={name}>
      {text}
    </label>
    <input
      className="label-input__input"
      type={type}
      name={name}
      onChange={onChange}
    />
  </div>
);

export default LabelInput;
