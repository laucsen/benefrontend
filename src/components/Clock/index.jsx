import React from 'react';
import PropTypes from 'prop-types';

import './index.scss';

class Clock extends React.Component {
  constructor(props) {
    super(props);

    const { hm } = props;
    
    this.state = {
      hours: hm[0],
      minutes: hm[1],
    };
  }

  componentDidMount() {
    setTimeout(this.start.bind(this), 1000 * 60);
  }

  start() {
    const { hours, minutes } = this.state;
    let nm = minutes + 1;
    let nh = hours;
    if (nm === 60) {
      nm = 0;
      nh += 1;
    }
    if (nh === 24) {
      nh = 0;
    }
    this.setState({
      hours: nh,
      minutes: nm,
    });
    setTimeout(this.start.bind(this), 1000 * 60);
  }

  render() {
    const formmatDigit = (number) => {
      const val = number.toString();
      if (val.length < 2) {
        return `0${val}`;
      }
      return val;
    };

    const { hours, minutes } = this.state;
    return (
      <div className="clock">
        <div className="clock__digit">{formmatDigit(hours)}</div>
        <div className="clock__digit">{formmatDigit(minutes)}</div>
      </div>
    );
  }
}

export default Clock;
