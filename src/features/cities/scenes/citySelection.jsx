import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import WeatherSpinner from '../../../components/WeatherSpinner';

import { getCities } from '../actions';

import './citySelection.scss';

class CitySelection extends Component {
  componentDidMount() {
    const { getCities } = this.props;
    getCities();
  }

  render() {
    const { cities, loading } = this.props;

    if (loading) {
      return (<WeatherSpinner />);
    }

    return (
      <div className="city-selection">
        <div className="city-selection__fit-area">
          <div>
            {cities.map(city => (
              <div className="city-selection__clickable_city" key={city.id}>
                <Link to={`/info/${city.name}/`}>
                  <div className="city-selection__city">{city.name}</div>
                </Link>
              </div>
            ))}
          </div>
          <div className="city-selection__interaction-area">
            <Link to="/createcity/">
              <FontAwesomeIcon
                color="#82C91E"
                size="2x"
                icon="plus"
              />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

CitySelection.propTypes = {
  cities: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
  })).isRequired,
  loading: PropTypes.bool.isRequired,
  getCities: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  cities: state.cities.cities,
  loading: state.cities.loading,
});

const mapDispatchToProps = dispatch => ({
  getCities: () => dispatch(getCities()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CitySelection));
