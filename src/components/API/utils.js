// Helpers
export const accessToken = ({ auth }) => {
  if (auth) {
    return auth.token;
  }
  return '';
};

/**
 * Atach auth token to headers.
 * @param headers
 * @returns {function(*=): {Authorization: string}}
 */
export const withAuth = (headers = {}) => state => ({
  ...headers,
  Authorization: `Bearer ${accessToken(state)}`,
});
