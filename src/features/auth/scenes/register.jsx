import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import ReturnButton from '../../../components/ReturnButton';
import LabelInput from '../../../components/LabelInput';
import LabelSelectable from '../../../components/LabelSelectable';

import { register } from '../actions';

import './register.scss';

class RegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: '',
      password2: '',
      country: '',
    };
  }

  getForm() {
    const { country } = this.state;

    return (
      <div className="rgister-scene">
        <form
          className="register-form"
          onSubmit={(e) => {
            this.handleSubmit(e);
          }}
        >
          <LabelInput
            type="text"
            text="Username"
            name="userName"
            onChange={(ev) => {
              this.handleFieldChange('userName', ev);
            }}
          />
          <LabelInput
            type="password"
            text="Password"
            name="password"
            onChange={(ev) => {
              this.handleFieldChange('password', ev);
            }}
          />
          <LabelInput
            type="password"
            text="Confirmation"
            name="password2"
            onChange={(ev) => {
              this.handleFieldChange('password2', ev);
            }}
          />
          <div className="rgister-scene--padded">
            <LabelSelectable
              text="Country"
              name="country"
              value={country}
              onChange={(value) => {
                this.handleFieldChange('country', { target: { value } });
              }}
            />
          </div>
          <div>
            <button className="login-scene__button">Register</button>
          </div>
        </form>
      </div>
    );
  }

  handleFieldChange(attr, ev) {
    const shatter = {};
    shatter[attr] = ev.target.value;
    this.setState((prevState) => ({
      ...prevState,
      ...shatter,
    }));
  }

  handleSubmit(e) {
    e.preventDefault();

    const { register } = this.props;
    const { userName, password, password2, country } = this.state;
    register(userName, password, password2, country);
  }

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" push />;
    }
    return (
      <div className={'login-area'}>
        <ReturnButton  to="/" />
        <div>{this.getForm()}</div>
      </div>
    );
  }
}

/* Notice how we don't pass the second parameter ownProps here, because it's not necessary. */
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isLogged,
  loginError: state.auth.loginError,
});

const mapDispatchToProps = (dispatch) => ({
  register: (username, password, password2, country) =>
    dispatch(register(username, password, password2, country)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterForm);
