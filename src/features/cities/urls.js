import APP_URL from '../../config/appurl';

export const CITIES = `${APP_URL}/private/cities/my`;
export const CAPITALS = `${APP_URL}/public/geolocation/capitals`;
export const SAVE_CITY = `${APP_URL}/private/cities`;
export const WEATHER = `${APP_URL}/private/cities/info/`;
