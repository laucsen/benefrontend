import { createAction } from 'redux-actions';
import { toast } from 'react-toastify';

import { LOGIN, REGISTER } from './urls';
import API from '../../components/API';

// Actions we dispatch in our Action Creators
export const validateFailed = createAction('AUTH/VALIDATE_FAILED');

export const registerRequested = createAction('AUTH/REGISTER_REQUEST');
export const registerFailed = createAction('AUTH/REGISTER_FAILED');

export const loginRequested = createAction('AUTH/LOGIN_REQUEST');
export const loginSucceeded = createAction('AUTH/LOGIN_SUCCEEDED');
export const loginFailed = createAction('AUTH/LOGIN_FAILED');

// Action Creators

export const invalidateAuth = () => async dispatch => dispatch(validateFailed());

export const register = (username, password, password2, country) => async (dispatch) => {
  dispatch(registerRequested({ username, password }));
  const body = await API.post(REGISTER, {
    body: {
      email: username,
      password,
      password2,
      country,
    },
  });
  if (body.error) {
    toast.error('Impossible to register this user', { position: toast.POSITION.TOP_CENTER });
    return dispatch(registerFailed({ error: body.error }));
  }
  return dispatch(loginSucceeded({
    user: body.user,
    token: body.token,
  }));
};

export const login = (username, password) => async (dispatch) => {
  dispatch(loginRequested({ username, password }));
  const body = await API.post(LOGIN, { body: { email: username, password } });
  if (body.error) {
    toast.error('Invalid Login or Password', { position: toast.POSITION.TOP_CENTER });
    return dispatch(loginFailed({ error: body.error }));
  }
  return dispatch(loginSucceeded({
    user: body.user,
    token: body.token,
  }));
};
