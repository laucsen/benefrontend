import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import * as serviceWorker from './config/serviceWorker';

import WeatherRouter from './router';

import fonts from './config/fonts';

fonts();

ReactDOM.render(<WeatherRouter />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
