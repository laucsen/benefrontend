import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './App';

import PrivateRoute from './components/PrivateRoute';

import Login from './features/auth/scenes/login';
import Register from './features/auth/scenes/register';
import CitySelection from './features/cities/scenes/citySelection';
import CityCreation from './features/cities/scenes/cityCreation';
import CityInfo from './features/cities/scenes/cityInfo';

import store from './config/create-store';

class WeatherRouter extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <App>
            <Switch>
              <PrivateRoute exact path="/" component={CitySelection} />
              <PrivateRoute exact path="/createcity/" component={CityCreation} />
              <PrivateRoute exact path="/info/:city" component={CityInfo} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
            </Switch>
          </App>
        </Router>
      </Provider>
    );
  }
}

export default WeatherRouter;
