/* eslint-disable */
import fetch from 'isomorphic-fetch';
import { push } from 'react-router-redux';
import Cookies from 'js-cookie';

import { withAuth } from './utils';

async function request(url, userOptions, dispatch, store) {
  const defaultOptions = {
    credentials: 'same-origin',
    headers: withAuth({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Content-Security-Policy': "default-src 'self'",
      'X-Frame-Options': 'SAMEORIGIN',
      'X-XSS-Protection': 1,
      'X-CSRFToken': Cookies.get('csrftoken'),
    })(store.getState()),
  };

  if (userOptions.body && typeof userOptions.body === 'object') {
    userOptions.body = JSON.stringify(userOptions.body);
  }

  const options = { ...defaultOptions, ...userOptions };

  try {
    const response = await fetch(url, options);
    const body = await response.json();

    if (body.redirectTo) {
      if (!dispatch) {
        throw new Error('Dispatch not found!');
      }

      dispatch(push(body.redirectTo));
    }

    if (response.ok) {
      return body;
    }

    throw body;
  } catch (e) {
    const error = 'An unexpected error has occurred!';
    return { error };
  }
}

class API {
  store = null;

  constructor() {
    ['get', 'post', 'put', 'delete'].forEach((method) => {
      this[method] = (url, options = {}) => {
        options.method = method;
        return request(url, options, this.getDispatch(), this.getStore());
      };
    });
  }

  setStore(store) {
    this.store = store;
  }

  getStore() {
    return this.store;
  }

  getDispatch() {
    return this.store.dispatch;
  }
}

export default new API();
