import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link, withRouter } from 'react-router-dom';

import { login } from '../actions';

import LabelInput from '../../../components/LabelInput';

import './login.scss';

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: '',
    };
  }

  handleFieldChange(attr, ev) {
    const state = this.state;
    state[attr] = ev.target.value;
    this.setState(state);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.login(this.state.userName, this.state.password);
  }

  getForm() {
    return (
      <div className="login-scene">
        <div className="login-scene__form">
          <form
            onSubmit={(e) => {
              this.handleSubmit(e);
            }}>
            <LabelInput
              text="Username"
              type="text"
              name="userName"
              onChange={(ev) => {
                this.handleFieldChange('userName', ev);
              }}
            />

            <LabelInput
              text="Password"
              type="password"
              name="password"
              onChange={(ev) => {
                this.handleFieldChange('password', ev);
              }}
            />

            <div className="login-scene__row">
              <button className="login-scene__button login-scene__button--fix">Login</button>
            </div>
          </form>

          <div className="login-scene__row">
            <Link to={`/register`}>
              <button className="login-scene__button">Register</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" push />;
    }
    return <div>{this.getForm()}</div>;
  }
}

/* Notice how we don't pass the second parameter ownProps here, because it's not necessary. */
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isLogged,
  loginError: state.auth.loginError,
});

const mapDispatchToProps = (dispatch) => ({
  login: (username, password) => dispatch(login(username, password)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm));
