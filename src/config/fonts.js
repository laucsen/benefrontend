import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPlus,
  faLessThan,
  faTemperatureLow,
  faSun,
  faMoon,
} from '@fortawesome/free-solid-svg-icons';

export default () => {
  library.add(faPlus);
  library.add(faLessThan);
  library.add(faTemperatureLow);
  library.add(faSun);
  library.add(faMoon);
};
