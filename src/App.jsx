import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import ClipLoader from 'react-spinners/ClipLoader';

import 'react-toastify/dist/ReactToastify.css';
import './App.scss';

import { invalidateAuth } from './features/auth/actions';

class App extends Component {
  static skipLocation = ['/login'];

  componentDidMount() {
    const { invalidateAuth } = this.props;
    invalidateAuth();
  }

  isToSkip() {
    const pn = this.props.location.pathname;
    if (pn === '/') {
      return true;
    }

    return App.skipLocation.reduce((acumulador, valorAtual) => {
      if (acumulador) {
        return acumulador;
      }
      return pn.indexOf(valorAtual) === 0;
    }, false);
  }

  render() {
    const { isAuthenticating } = this.props;
    return (
      <div className="App">
        <div className="App__content">
          {isAuthenticating && !this.isToSkip() ? (
            <div className="group">
              <div>
                <ClipLoader
                  sizeUnit={'px'}
                  size={150}
                  color={'#123abc'}
                  loading={true}
                />
              </div>
            </div>
          ) : (
              this.props.children
            )}
        </div>
        <ToastContainer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticating: state.auth.isAuthenticating
});

const mapDispatchToProps = dispatch => ({
  invalidateAuth: () => dispatch(invalidateAuth()),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(App)
);
