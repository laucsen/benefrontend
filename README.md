# Weather 101 - Frontend

> Try it [here](http://weather-101.surge.sh)

### Requirements

1. Nodejs
2. Npm

**Note**: This project was bootstraped with react apps.
**Note**: Was added node-sass to use `scss`.

### Installation

1. Navigate to front end project folder.
2. Run `npm i`
3. Run `npm start`

### Deploy

Hosted with Surge.sh

1. run `npm run build`
2. Navigate to `build` folder
3. run `surge'
4. domain -> `weather-101.surge.sh`

### About Weather 101

1. On default route `/`, application will show a list of user capitals. If user is not logged, application will redirect the user to login screen. On add button, user can go to a screen to register new cities.
2. On login screen `/login`, user can log into application. After a sucessfull login, user will be redirected to default route `/`. If user is not registered, he can go to register screen by clicking on register ou accessing `/register`.
3. On register Screen `/register`, user can create an account, and if desired, user can set your country.
4. On Capital Creation Screen `/createcity`, user can select a capital on central input and save after select one city on input popover. When saving, user return to `/`.
5. On Capital Info Screen `/info/:city`, user can see weather information about `:city`, that is a selected city on main screen.

#### Notes

1. Weather-101 doe's not accept to access directly an route, like: /register. I don't know the reason, but running locally, it works.
2. To logout this app, you have to remove yor token from localstorage.
3. LabelSelectable is a `fast` solution to fetch contries. I decided to use such bad element because register screen was no required.
4. Also, Register screen is without a got layout. I didn't spendt my time on this screen.
5. As Capital selector will show only 8 cities, I decided to not create the caret button to open de list. On my implementation user have to start to digit his capital name and it will be shown on list.

### What could be better

1. Error messgeas. Now, each message is fixed on frontend. Ths right thing to is to listen for backend errors and write the right message. I didn't this becausa the dead line to deliver the test.
2. Improove Registerarion screen and LabelSelectable component.
3. Create a logout system.
4. Front end Test using enzyme and jest. I din't have enough time to test frontend application, as the backend is.
