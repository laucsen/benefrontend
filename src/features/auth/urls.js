import APP_URL from '../../config/appurl';

export const LOGIN = `${APP_URL}/public/login`;
export const REGISTER = `${APP_URL}/public/register`;
