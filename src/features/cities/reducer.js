import { handleActions } from 'redux-actions';
import {
  getCitiesRequested,
  getCitiesSucceded,
  resetCapitals,
  capitalsRequestSucceded,
  saveCapitalRequested,
  saveCapitalRequestSucceded,
  getWeatherRequested,
  getWeatherRequestSucceded,
} from './actions';

const defaultState = {
  cities: [],
  capitals: [],
  info: undefined,
  loading: false,
};

export default handleActions({
  [getCitiesRequested]: state => ({
    ...state,
    loading: true,
  }),
  [getCitiesSucceded]: (state, { payload }) => ({
    ...state,
    cities: payload.cities,
    loading: false,
  }),
  [resetCapitals]: state => ({
    ...state,
    capitals: [],
  }),
  [capitalsRequestSucceded]: (state, { payload }) => ({
    ...state,
    capitals: payload,
  }),
  [saveCapitalRequested]: state => ({
    ...state,
    loading: true,
  }),
  [saveCapitalRequestSucceded]: state => ({
    ...state,
    loading: false,
  }),
  [getWeatherRequested]: state => ({
    ...state,
    info: undefined,
    loading: true,
  }),
  [getWeatherRequestSucceded]: (state, { payload }) => ({
    ...state,
    info: payload,
    loading: false,
  }),
}, defaultState);
