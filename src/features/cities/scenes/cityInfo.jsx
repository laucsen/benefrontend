import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment-timezone';

import ReturnButton from '../../../components/ReturnButton';
import Clock from '../../../components/Clock';
import WeatherSpinner from '../../../components/WeatherSpinner';

import { getWeather } from '../actions';

import './cityInfo.scss';

const ICON_MAP = {
  default: 'wi-alien',
  'clear': 'wi-day-sunny',
  'clear sky': 'wi-day-sunny',
  'few clouds': 'wi-day-cloudy',
  'scattered clouds': 'wi-cloud',
  'broken clouds': 'wi-cloudy',
  'shower rain': 'wi-showers',
  rain: 'wi-day-hail',
  thunderstorm: 'wi-lightning',
  snow: 'wi-snow',
  mist: 'wi-smog',
  'light rain': 'wi-hail',
};

class CityInfo extends Component {
  static formmatHour(date) {
    const twodig = (n) => {
      const r = `${n}`;
      if (r.length === 1) {
        return `0${r}`;
      }
      return r;
    };
    return `${twodig(date[0])}:${twodig(date[1])}`;
  }

  static getIcon(i) {
    return ICON_MAP[i] === undefined ? ICON_MAP.default : ICON_MAP[i];
  }

  static getHour(hstr) {
    const spr = hstr.split(':');
    return [
      parseInt(spr[0]),
      parseInt(spr[1]),
    ];
  }

  componentDidMount() {
    const { getWeather, match } = this.props;
    getWeather(match.params.city);
  }

  getBlockWeather() {
    const { info } = this.props;
    const { weather } = info;
    console.log(weather);
    return (
      <div className="city-info__block">
        <div className="city-info__icon">
          <i className={`wi ${CityInfo.getIcon(weather)}`}></i>
        </div>
        <div className="city-info__font--tiny city-info--celeste">
          {weather}
        </div>
      </div>
    );
  }

  getHourAndName() {
    const { info } = this.props;
    const { timezone } = info;

    const now = moment.tz(Date.now(), timezone);
    const hm = CityInfo.getHour(now.format('HH:mm'));

    return (
      <div className="city-info__block">
        <Clock hm={hm} />
        <div className="city-info__city-name city-info__font--large city-info--sky">{info.name}</div>
      </div>
    );
  }

  getTemperatureBlock() {
    const computeTemp = (value) => {
      const KELVIN = 273.15;
      const result = value - KELVIN;
      return result.toFixed(0).toString();
    };
    const { info } = this.props;
    const {
      sunrise,
      sunset,
      temp,
      timezone,
    } = info;

    const sDate = moment.tz(sunrise * 1000, timezone);
    const dDate = moment.tz(sunset * 1000, timezone);

    const hmS = CityInfo.getHour(sDate.format('HH:mm'));
    const hmD = CityInfo.getHour(dDate.format('HH:mm'));

    return (
      <div className="city-info__block">
        <div className="city-info__temperature-block">
          <FontAwesomeIcon
            color="#013876"
            size="2x"
            icon="temperature-low"
          />
          <div className="city-info__temp-item city-info__font--normal city-info--blue city-info__font--lighter">{computeTemp(temp)}&deg;C</div>
        </div>
        <div className="city-info__temperature-block">
          <FontAwesomeIcon
            color="#013876"
            size="2x"
            icon="sun"
          />
          <div className="city-info__temp-item city-info__font--normal-short city-info--blue city-info__font--lighter">{CityInfo.formmatHour(hmS)}</div>
        </div>
        <div className="city-info__temperature-block">
          <FontAwesomeIcon
            color="#013876"
            size="2x"
            icon="moon"
          />
          <div className="city-info__temp-item city-info__font--normal-short city-info--blue city-info__font--lighter">{CityInfo.formmatHour(hmD)}</div>
        </div>
      </div>
    );
  }

  render() {
    const { info, loading } = this.props;
    if (info === undefined || loading) {
      return (<WeatherSpinner />);
    }

    return (
      <div className="city-info">
        <ReturnButton to="/" />
        <div className="city-info__row-area">
          {this.getBlockWeather()}
          {this.getHourAndName()}     
          {this.getTemperatureBlock()}
        </div>
      </div>
    );
  }
}

CityInfo.propTypes = {
  getWeather: PropTypes.func.isRequired,
  info: PropTypes.object,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  info: state.cities.info,
  loading: state.cities.loading,
});

const mapDispatchToProps = dispatch => ({
  getWeather: (city) => dispatch(getWeather(city)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CityInfo));
